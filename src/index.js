import { auth } from './utilities/authentication'
import { contacts } from './utilities/contacts'
import { configuration } from './config/config';

(async () => {
  try {
    configuration.API_TOKEN = auth.authToken()
    console.log('***************LISTA CONTACTOS 1***************')
    contacts.listarContactos(configuration.API_TOKEN.access_token)
    contacts.agregarContacto(configuration.API_TOKEN.access_token)
    console.log('***************LISTA CONTACTOS 2***************')
    contacts.listarContactos(configuration.API_TOKEN.access_token)
  } catch (error) {
    process.exit(1)
  }


})()
